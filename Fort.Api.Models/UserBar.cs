﻿using System;

namespace Fort.Api.Models
{
    public class UserBar
    {
        public string FavoriteDrink { get; set; }   

        public Guid UserId { get; set; }

        public Guid BarId { get; set; }

        public User User { get; set; }

        public Bar Bar { get; set; }
    }
}
