﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Fort.Api.Models
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        [JsonIgnore]
        public string Password { get; set; } // needs to be hashed

        public IList<UserCity> UserCities { get; set; }
        public IList<UserBar> UserBars { get; set; }
    }
}