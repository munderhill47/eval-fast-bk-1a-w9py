﻿using System;

namespace Fort.Api.Models
{
    public class UserCity
    {
        public Guid UserId { get; set; }

        public Guid CityId { get; set; }

        public User User { get; set; }

        public City City { get; set; }

    }
}
