﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Fort.Api.Models
{
    public class City
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }

        // Navigation property
        [JsonIgnore]
        public virtual List<UserCity> UserCities { get; set; }

    }
}
