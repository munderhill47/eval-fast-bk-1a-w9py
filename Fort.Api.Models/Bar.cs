﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Fort.Api.Models
{
    public class Bar
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Guid CityId { get; set; }

        [ForeignKey("CityId")]
        public City City { get; set; }

        // Navigation property
        [JsonIgnore]
        public virtual IList<UserBar> UserBars { get; set; }
    }
}
