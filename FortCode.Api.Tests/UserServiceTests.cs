﻿using Fort.Api.Models;
using FortCode.Interfaces;
using FortCode.Models.Requests;
using FortCode.Services;
using Microsoft.AspNet.Identity;
using Moq;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace FortCode.Api.Tests
{
    [ExcludeFromCodeCoverage]
    public class UserServiceTests
    {
        private Mock<IUserRepository> _repository = new Mock<IUserRepository>();

        public UserServiceTests()
        {
        }

        [Fact]
        public void CreateUserShouldFailNameValidation()
        {
            var service = new UserService(_repository.Object);

            var request = new CreateUserRequest()
            {
                Email = "test@fort.com",
                Password = "p@ssw0rd"
            };

            var response = service.Create(request);

            Assert.False(response.Success);
            Assert.Equal("Create user request was not valid!", response.Error);
        }

        [Fact]
        public void CreateUserShouldFailEmailValidation()
        {
            var service = new UserService(_repository.Object);

            var request = new CreateUserRequest()
            {
                Name = "Test User",
                Password = "p@ssw0rd"
            };

            var response = service.Create(request);

            Assert.False(response.Success);
            Assert.Equal("Create user request was not valid!", response.Error);
        }

        [Fact]
        public void CreateUserShouldFailPasswordValidation()
        {
            var service = new UserService(_repository.Object);

            var request = new CreateUserRequest()
            {
                Name = "Test User",
                Email = "test@fort.com",
            };

            var response = service.Create(request);

            Assert.False(response.Success);
            Assert.Equal("Create user request was not valid!", response.Error);
        }

        [Fact]
        public void CreateUserShouldSucceed()
        {
            _repository.Setup(r => r.GetUserByEmail(It.IsAny<string>())).Returns((User)null);

            var service = new UserService(_repository.Object);

            var request = new CreateUserRequest()
            {
                Name = "Test User",
                Email = "test@fort.com",
                Password = "p@ssw0rd"
            };

            var response = service.Create(request);

            Assert.True(response.Success);

            Assert.Equal("Test User", response.User.Name);
            Assert.Equal("test@fort.com", response.User.Email);
            Assert.True(VerifyPassword(response.User.Password, "p@ssw0rd"));
            Assert.NotEqual(Guid.Empty, response.User.Id);
        }

        [Fact]
        public void DuplicateUserEmailShouldFail()
        {
            var existingUser = new User() { Email = "test@fort.com", Name = "Test User" };

            _repository.Setup(r => r.GetUserByEmail(It.IsAny<string>())).Returns(existingUser);

            var service = new UserService(_repository.Object);

            var request = new CreateUserRequest()
            {
                Name = "Test User",
                Email = "test@fort.com",
                Password = "p@ssw0rd"
            };

            var response = service.Create(request);

            Assert.False(response.Success);
            Assert.Equal("A user with that E-mail already exists!", response.Error);
        }

        [Fact]
        public void AuthenticateUserShouldFailEmailValidation()
        {
            var service = new UserService(_repository.Object);

            var request = new AuthenticateRequest()
            {
                Password = "p@ssw0rd"
            };

            var response = service.Authenticate(request);

            Assert.False(response.Success);
            Assert.Equal("Authenticate user request was not valid!", response.Error);
        }

        [Fact]
        public void AuthenticateUserShouldFailPasswordValidation()
        {
            var service = new UserService(_repository.Object);

            var request = new AuthenticateRequest()
            {
                Email = "test@fort.com",
            };

            var response = service.Authenticate(request);

            Assert.False(response.Success);
            Assert.Equal("Authenticate user request was not valid!", response.Error);
        }

        [Fact]
        public void AuthenticateUserShouldSucceed()
        {
            var existingUser = new User() { Email = "test@fort.com", Name = "Test User", Id = Guid.NewGuid(), Password = HashPassword("p@ssw0rd") };

            _repository.Setup(r => r.GetUserByEmail(It.IsAny<string>())).Returns(existingUser);

            var service = new UserService(_repository.Object);

            var request = new AuthenticateRequest()
            {
                Email = "test@fort.com",
                Password = "p@ssw0rd"
            };

            var response = service.Authenticate(request);

            Assert.True(response.Success);
            Assert.Null(response.Error);
            Assert.NotNull(response.Token);
            Assert.NotEqual(string.Empty, response.Token);
        }

        [Fact]
        public void AuthenticateUserShouldFailWithWrongEmail()
        {
            _repository.Setup(r => r.GetUserByEmail(It.IsAny<string>())).Returns((User)null);

            var service = new UserService(_repository.Object);

            var request = new AuthenticateRequest()
            {
                Email = "test@fort.com",
                Password = "p@ssw0rd"
            };

            var response = service.Authenticate(request);

            Assert.False(response.Success);
            Assert.Null(response.Token);
            Assert.Equal("E-mail or Password were not correct!", response.Error);
        }

        [Fact]
        public void AuthenticateUserShouldFailWithWrongPassword()
        {
            var existingUser = new User() { Email = "test@fort.com", Name = "Test User", Id = Guid.NewGuid(), Password = HashPassword("p@ssw0rd!") };

            _repository.Setup(r => r.GetUserByEmail(It.IsAny<string>())).Returns(existingUser);

            var service = new UserService(_repository.Object);

            var request = new AuthenticateRequest()
            {
                Email = "test@fort.com",
                Password = "p@ssw0rd"
            };

            var response = service.Authenticate(request);

            Assert.False(response.Success);
            Assert.Null(response.Token);
            Assert.Equal("E-mail or Password were not correct!", response.Error);
        }

        [Fact]
        public void GetUserByIdShouldFailWithInvalidId()
        {
            // NOTE: This is only used by the Authentication Middleware
            _repository.Setup(r => r.GetUserById(It.IsAny<Guid>())).Returns((User)null);

            var service = new UserService(_repository.Object);

            var user = service.GetById(Guid.Empty);

            Assert.Null(user);
        }

        [Fact]
        public void GetUserByIdShouldSucceedWithValidId()
        {
            var existingUser = new User() { Email = "test@fort.com", Name = "Test User", Id = Guid.NewGuid(), Password = HashPassword("p@ssw0rd") };

            // NOTE: This is only used by the Authentication Middleware
            _repository.Setup(r => r.GetUserById(It.IsAny<Guid>())).Returns(existingUser);

            var service = new UserService(_repository.Object);

            var user = service.GetById(existingUser.Id);

            Assert.NotNull(user);
        }

        private string HashPassword(string password)
        {
            var hasher = new PasswordHasher();

            return hasher.HashPassword(password);
        }

        private bool VerifyPassword(string hashedPassword, string password)
        {
            var hasher = new PasswordHasher();

            return hasher.VerifyHashedPassword(hashedPassword, password) == PasswordVerificationResult.Success;
        }
    }
}
