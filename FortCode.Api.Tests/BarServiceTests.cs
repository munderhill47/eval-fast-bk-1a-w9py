﻿using Fort.Api.Models;
using FortCode.Interfaces;
using FortCode.Models.Requests;
using FortCode.Services;
using Moq;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace FortCode.Api.Tests
{
    [ExcludeFromCodeCoverage]
    public class BarServiceTests
    {
        private Mock<IBarRepository> _repository = new Mock<IBarRepository>();

        [Fact]
        public void CreateBarShouldFailNameValidation()
        {
            var service = new BarService(_repository.Object);

            var request = new CreateBarRequest()
            {
                Name = "",
                FavoriteDrink = "Beer"
            };

            var response = service.Create(Guid.NewGuid(), Guid.NewGuid(), request);

            Assert.False(response.Success);
            Assert.Equal("Create Bar request was not valid!", response.Error);
        }

        [Fact]
        public void CreateCountryShouldFailFavoriteDrinkValidation()
        {
            var service = new BarService(_repository.Object);

            var request = new CreateBarRequest()
            {
                Name = "Mavericks 321",
            };

            var response = service.Create(Guid.NewGuid(), Guid.NewGuid(), request);

            Assert.False(response.Success);
            Assert.Equal("Create Bar request was not valid!", response.Error);
        }

        [Fact]
        public void CreateBarShouldSucceed()
        {
            _repository.Setup(r => r.GetBarByNameInCity(It.IsAny<Guid>(), It.IsAny<string>())).Returns((Bar)null);

            var service = new BarService(_repository.Object);

            var request = new CreateBarRequest()
            {
                Name = "Mavericks 321",
                FavoriteDrink = "Beer"
            };

            var response = service.Create(Guid.NewGuid(), Guid.NewGuid(), request);

            Assert.True(response.Success);

            Assert.Equal("Mavericks 321", response.Bar.Name);
            Assert.NotEqual(Guid.Empty, response.Bar.Id);
        }

        [Fact]
        public void DeleteBarShouldSucceed()
        {
            var existingBar = new Bar() { Id = Guid.NewGuid() };

            _repository.Setup(r => r.GetBarById(It.IsAny<Guid>())).Returns((Bar)existingBar);

            var service = new BarService(_repository.Object);

            var response = service.Delete(Guid.NewGuid(), existingBar.Id);

            Assert.True(response.Success);
        }

        [Fact]
        public void DeleteBarShouldFailForInvalidBarId()
        {
            var service = new BarService(_repository.Object);

            var response = service.Delete(Guid.NewGuid(), Guid.Empty);

            Assert.False(response.Success);
            Assert.Equal("Invalid bar id!", response.Error);
        }

        [Fact]
        public void DeleteBarShouldFailForNonExistingBarId()
        {
            var existingBar = new Bar() { Id = Guid.NewGuid() };

            _repository.Setup(r => r.GetBarById(It.IsAny<Guid>())).Returns((Bar)null);

            var service = new BarService(_repository.Object);

            var response = service.Delete(Guid.NewGuid(), existingBar.Id);

            Assert.False(response.Success);
            Assert.Equal("A bar with that id does not exist!", response.Error);
        }
    }
}