﻿using Fort.Api.Models;
using FortCode.Interfaces;
using FortCode.Models.Requests;
using FortCode.Services;
using Moq;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace FortCode.Api.Tests
{
    [ExcludeFromCodeCoverage]
    public class CityServiceTests
    {
        private Mock<ICityRepository> _repository = new Mock<ICityRepository>();

        [Fact]
        public void CreateCityShouldFailNameValidation()
        {
            var service = new CityService(_repository.Object);

            var request = new CreateCityRequest()
            {
                Name = "",
                Country = "United States"
            };

            var response = service.Create(Guid.NewGuid(), request);

            Assert.False(response.Success);
            Assert.Equal("Create city request was not valid!", response.Error);
        }

        [Fact]
        public void CreateCountryShouldFailCountryValidation()
        {
            var service = new CityService(_repository.Object);

            var request = new CreateCityRequest()
            {
                Name = "Satellite Beach",
                Country = ""
            };

            var response = service.Create(Guid.NewGuid(), request);

            Assert.False(response.Success);
            Assert.Equal("Create city request was not valid!", response.Error);
        }

        [Fact]
        public void CreateCityShouldSucceed()
        {
            _repository.Setup(r => r.GetCityByNameAndCountry(It.IsAny<string>(), It.IsAny<string>())).Returns((City)null);

            var service = new CityService(_repository.Object);

            var request = new CreateCityRequest()
            {
                Name = "Satellite Beach",
                Country = "United States",
            };

            var response = service.Create(Guid.NewGuid(), request);

            Assert.True(response.Success);

            Assert.Equal("Satellite Beach", response.City.Name);
            Assert.Equal("United States", response.City.Country);
            Assert.NotEqual(Guid.Empty, response.City.Id);
        }

        [Fact]
        public void DeleteCityShouldSucceed()
        {
            var existingCity = new City() { Id = Guid.NewGuid() };

            _repository.Setup(r => r.GetCityById(It.IsAny<Guid>())).Returns((City)existingCity);

            var service = new CityService(_repository.Object);

            var response = service.Delete(Guid.NewGuid(), existingCity.Id);

            Assert.True(response.Success);
        }

        [Fact]
        public void DeleteCityShouldFailForCityIdValidation()
        {
            var service = new CityService(_repository.Object);

            var response = service.Delete(Guid.NewGuid(), Guid.Empty);

            Assert.False(response.Success);
            Assert.Equal("Invalid city id!", response.Error);
        }

        [Fact]
        public void DeleteCityShouldFailForNonExistingCityId()
        {
            var existingCity = new City() { Id = Guid.NewGuid() };

            _repository.Setup(r => r.GetCityById(It.IsAny<Guid>())).Returns((City)null);

            var service = new CityService(_repository.Object);

            var response = service.Delete(Guid.NewGuid(), existingCity.Id);

            Assert.False(response.Success);
            Assert.Equal("A city with that id does not exist!", response.Error);
        }
    }
}
