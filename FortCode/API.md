# API Descriptions

- ### User API
  
	|/users|
	| - | - |
	| Description | Creates a new user if the submitted email does not already exist. |
	| Method | POST |
	| Parameters | CreateUserRequest (in Body) |
	| Returns | **201 (Created)** Indicates the user was successfully created and returns a CreateUserResponse. |
	| Errors | **400 (Bad Request)** Indicates that the submitted CreateUserRequest contained invalid or incomplete data or a user with specified email already exists.|

	| /users/authenticate |
	| - | - |
	| Description | Authenticates a user against the supplied emai/password. |
	| Method | POST |
	| Parameters | AuthenticateRequest |
	| Returns | **200 (OK)** Indicates the user was successfully authenticated and returns a AuthenticateResponse containing a user *token* to be supplied on all subsequent API calls. |
	| Errors | **400 (Bad Request)** Indicates that the submitted CreateUserRequest contained invalid or incomplete data or a user with specified email already exists.|

- ### City API ==(Requires user token in Authorize header)==

	| /cities |
	| - | - |
	| Description |Gets a list of the user's favorite cities. |
	| Method | GET |
	| Parameters | *(none)* |
	| Returns | **200 (OK)** A list of City objects. |
	| Errors | *(none)* |

	| /cities |
	| - | - |
	| Description | Creates a new city if the city/country do not already exist. |
	|| If the city/country already exists, then the user is associated with the city. |
	| Method | POST |
	| Parameters | CreateCityRequest (in Body) |
	| Returns | **201 (Created)** Indicates the city was successfully created and returns a CreateCityResponse. |
	| Errors | **400 (Bad Request)** Indicates that the submitted CreateCityRequest contained invalid or incomplete data.|

	| /cities/\{cityId\} |
	| - | - |
	| Description | Deletes the city identified by \{cityId\} if there are no other users associated with it.|
	|| If other users are associated with the city, then only the association for the calling user **(from Authorize header)** is deleted. |
	| Method | DELETE |
	| Parameters | *(none)* |
	| Returns | **204 (No Content)** Indicates the city and/or association was successfully deleted. |
	| Errors | **400 (Bad Request)** Indicates that the submitted \{cityId\} was invalid.|

- ### Bar API  ==(Requires user token in Authorize header)==

	| /cities/\{cityId\}/bars |
	| - | - |
	| Description | Gets a list of the user's favorite bars in the favorite city specified by \{cityId\}. |
	| Method | GET |
	| Parameters | *(none)* |
	| Returns | **200 (OK)** A list of BarDto objects. |
	| Errors | *(none)* |

	| /cities/\{cityId\}/bars |
	| - | - |
	| Description | Creates a new bar if the bar name do not already exist in the city specified by \{cityId\}. |
	| Method | POST |
	| Parameters | CreateBarRequest (in Body) |
	| Returns | **201 (Created)** Indicates the bar was successfully created and returns a CreateBarResponse. |
	| Errors | **400 (Bad Request)** Indicates that the submitted CreateBarRequest contained invalid or incomplete data.|

	| /cities/\{cityId\}/bars/\{barId\} |
	| - | - |
	| Description | Deletes the bar specified by \{barId\} in the city identified by \{cityId\} if there are no other users associated with it.|
	|| If other users are associated with the bar, then only the association for the calling user **(from Authorize header)** is deleted. |
	| Method | DELETE |
	| Parameters | *(none)* |
	| Returns | **204 (No Content)** Indicates the city and/or association was successfully deleted. |
	| Errors | **400 (Bad Request)** Indicates that the submitted \{cityId\} or \{barId\} was invalid.|

- ### Shared Bar API  ==(Requires user token in Authorize header)==

	| /sharedbars |
	| - | - |
	| Description | Gets a list of the user's favorite bars that are shared by other users. |
	| Method | GET |
	| Parameters | *(none)* |
	| Returns | **200 (OK)** A list of SharedBarDto objects. |
	| Errors | *(none)* |
