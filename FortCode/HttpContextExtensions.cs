﻿using Microsoft.AspNetCore.Http;

namespace FortCode
{
    public static class HttpContextExtensions
    {
        public static T GetItem<T>(this HttpContext context, object key)
        {
            var value = default(T);

            if (context.Items.ContainsKey(key))
            {
                value = (T)context.Items[key];
            }

            return value;
        }
    }
}
