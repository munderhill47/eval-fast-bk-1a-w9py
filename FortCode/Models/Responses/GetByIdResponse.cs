﻿namespace FortCode.Models.Responses
{
    public class GetByIdResponse<T> : ResponseBase
    {
        public T Entity { get; set; }
    }
}
