﻿using System.Collections.Generic;

namespace FortCode.Models.Responses
{
    public class ListResponse<T> : ResponseBase
    {
        public IEnumerable<T> Entities { get; set; }
    }
}
