﻿using Fort.Api.Models;
using FortCode.Models.Responses;

namespace FortCode.Models
{
    public class CreateUserResponse : ResponseBase
    {
        public User User { get; set; }
    }
}
