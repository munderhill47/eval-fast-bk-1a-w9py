﻿using Fort.Api.Models;
using FortCode.Models.Responses;

namespace FortCode.Models
{
    public class CreateBarResponse : ResponseBase
    {
        public Bar Bar { get; set; }
    }
}
