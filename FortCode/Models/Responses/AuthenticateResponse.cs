﻿namespace FortCode.Models.Responses
{
    public class AuthenticateResponse : ResponseBase
    {
        public string UserName { get; set; }
        public string Token { get; set; }
    }
}
