﻿
using Fort.Api.Models;
using FortCode.Models.Responses;

namespace FortCode.Models
{
    public class CreateCityResponse : ResponseBase
    {
        public City City { get; set; }
    }
}
