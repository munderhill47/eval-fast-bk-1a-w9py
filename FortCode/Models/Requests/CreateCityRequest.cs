﻿namespace FortCode.Models.Requests
{
    public class CreateCityRequest
    {
        public string Name { get; set; }
        public string Country { get; set; }
    }
}
