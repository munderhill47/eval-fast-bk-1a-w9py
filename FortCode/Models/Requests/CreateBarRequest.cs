﻿namespace FortCode.Models.Requests
{
    public class CreateBarRequest
    {
        public string Name { get; set; }
        public string FavoriteDrink { get; set; }
    }
}
