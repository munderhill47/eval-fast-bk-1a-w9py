﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{
    public class BarShareCount
    {
        [Key]
        public Guid BarId { get; set; }
        public int ShareCount { get; set; }
    }
}
