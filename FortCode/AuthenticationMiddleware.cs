﻿using FortCode.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IUserRepository userRespository)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
            {
                // Check if login is correct
                var userId = GetUserIdFromToken(token);

                if (userId != Guid.Empty)
                {
                    var user = userRespository.GetUserById(userId);

                    if (user != null)
                    {
                        context.Items["userId"] = user.Id;
                        context.Items["userName"] = user.Name;
                    }
                }
            }

            await _next.Invoke(context);
        }

        private Guid GetUserIdFromToken(string token)
        {
            var userId = Guid.Empty;

            try
            {
                var id = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                if (!string.IsNullOrWhiteSpace(id))
                {
                    userId = Guid.Parse(id);
                }
            }
            catch (Exception ex)
            {
                // Do nothing
                Console.WriteLine($"Exception {JsonExtensions.SerializeToJson(ex)}");
            }

            return userId;
        }
    }
}
