﻿using Fort.Api.Models;
using FortCode.Interfaces;
using FortCode.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using System;

namespace FortCode.Services
{
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;

        public CityService(ICityRepository repository)
        {
            _cityRepository = repository;
        }

        public ListResponse<City> GetList(Guid userId)
        {
            return new ListResponse<City>() { Entities = _cityRepository.GetFavoriteCities(userId), Success = true }; ;
        }

        public CreateCityResponse Create(Guid userId, CreateCityRequest createRequest)
        {
            var response = new CreateCityResponse();

            if (ValidateCreateRequest(createRequest))
            {
                var city = _cityRepository.GetCityByNameAndCountry(createRequest.Name, createRequest.Country);

                if (city == null)
                {
                    city = new City()
                    {
                        Id = Guid.NewGuid(),
                        Name = createRequest.Name,
                        Country = createRequest.Country,
                    };

                    _cityRepository.AddNew(userId, city);
                }
                else
                {
                    _cityRepository.AddUserAssociationToCity(userId, city.Id);
                }

                _cityRepository.SaveChanges();

                response.Success = true;
                response.City = city;
            }
            else
            {
                response.Error = "Create city request was not valid!";
            }
        
            return response;
        }

        private bool ValidateCreateRequest(CreateCityRequest request)
        {
            return !string.IsNullOrWhiteSpace(request.Name) && !string.IsNullOrWhiteSpace(request.Country);
        }

        public DeleteCityResponse Delete(Guid userId, Guid cityId)
        {
            var response = new DeleteCityResponse();

            if (ValidateId(cityId))
            {
                var city = _cityRepository.GetCityById(cityId);

                if (city != null)
                {
                    _cityRepository.Delete(userId, city);

                    _cityRepository.SaveChanges();

                    response.Success = true;
                }
                else 
                {
                    response.Error = "A city with that id does not exist!";
                }
            }
            else
            {
                response.Error = "Invalid city id!";
            }

            return response;
        }

        private bool ValidateId(Guid id)
        {
            return id != null && id != Guid.Empty;
        }
    }
}
