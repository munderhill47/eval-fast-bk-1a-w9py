﻿using Fort.Api.Models;
using FortCode.DTOs;
using FortCode.Interfaces;
using FortCode.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using FortCode.Repositories;
using System;

namespace FortCode.Services
{
    public class BarService : IBarService
    {
        private readonly IBarRepository _barRepository;

        public BarService(IBarRepository barRepository)
        {
            _barRepository = barRepository;
        }

        public ListResponse<BarDto> GetListForCity(Guid userId, Guid cityId)
        {
            return new ListResponse<BarDto>()
            {
                Entities = _barRepository.GetFavoriteBarsInCity(userId, cityId),
                Success = true
            };
        }

        public ListResponse<SharedBarDto> GetSharedListForUser(Guid userId)
        {
            return new ListResponse<SharedBarDto>() { Entities = _barRepository.GetSharedBarsForUser(userId), Success = true };
        }

        public CreateBarResponse Create(Guid userId, Guid cityId, CreateBarRequest createRequest)
        {
            var response = new CreateBarResponse();

            if (ValidateCreateRequest(createRequest))
            {
                var bar = _barRepository.GetBarByNameInCity(cityId, createRequest.Name);

                if (bar == null)
                {
                    bar = new Bar()
                    {
                        Id = Guid.NewGuid(),
                        Name = createRequest.Name,
                        CityId = cityId,
                    };

                    _barRepository.AddNew(userId, bar, createRequest.FavoriteDrink);
                }
                else
                {
                    _barRepository.AddUserAssociationToBar(userId, bar.Id, createRequest.FavoriteDrink);
                }

                _barRepository.SaveChanges();

                response.Success = true;
                response.Bar = bar;
            }
            else
            {
                response.Error = "Create Bar request was not valid!";
            }

            return response;
        }

        private bool ValidateCreateRequest(CreateBarRequest request)
        {
            return !string.IsNullOrWhiteSpace(request.Name) && !string.IsNullOrWhiteSpace(request.FavoriteDrink);
        }

        public DeleteBarResponse Delete(Guid userId, Guid barId)
        {
            var response = new DeleteBarResponse();

            if (ValidateId(barId))
            {
                var bar = _barRepository.GetBarById(barId);

                if (bar != null)
                {
                    _barRepository.Delete(userId, bar);

                    _barRepository.SaveChanges();

                    response.Success = true;
                }
                else
                {
                    response.Error = "A bar with that id does not exist!";
                }
            }
            else
            {
                response.Error = "Invalid bar id!";
            }

            return response;

        }

        private bool ValidateId(Guid id)
        {
            return id != null && id != Guid.Empty;
        }
    }
}
