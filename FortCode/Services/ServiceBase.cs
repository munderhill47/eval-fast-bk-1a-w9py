﻿using Fort.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace FortCode.Services
{
    public abstract class ServiceBase
    {
        protected DbSet<User> _users => _dbContext.Users;
        protected DbSet<City> _cities => _dbContext.Cities;
        protected DbSet<Bar> _bars => _dbContext.Bars;
        protected DbSet<UserBar> _userBars => _dbContext.UserBars;
        protected DbSet<UserCity> _userCities => _dbContext.UserCities;

        private ApiDataContext _dbContext { get; }

        public ServiceBase(ApiDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        protected void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        protected void ExecuteInTransaction<T>(Func<T> operations, Action<T> onCommit = null, Action<T> onRollback = null, bool useOuterTransaction = false)
        {
            var transaction = useOuterTransaction ? _dbContext.Database.BeginTransaction() : default(IDbContextTransaction);

            T value = default(T);

            try
            {
                value = operations();

                transaction?.Commit();

                onCommit?.Invoke(value);
            }
            catch (Exception ex)
            {
                transaction?.Rollback();

                onRollback?.Invoke(value);

                // Log error
            }
            finally
            {
                transaction?.Dispose();
            }
        }

        protected void ExecuteInTransaction(Action operations, Action onCommit = null, Action onRollback = null, bool useOuterTransaction = false)
        {
            var transaction = useOuterTransaction ? _dbContext.Database.BeginTransaction() : default(IDbContextTransaction);

            try
            {
                operations();

                transaction?.Commit();

                onCommit?.Invoke();
            }
            catch (Exception ex)
            {
                transaction?.Rollback();

                onRollback?.Invoke();

                // Log error
            }
            finally
            {
                transaction?.Dispose();
            }
        }
    }
}
