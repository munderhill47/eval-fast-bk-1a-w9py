﻿using Fort.Api.Models;
using FortCode.Interfaces;
using FortCode.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using FortCode.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Text;

namespace FortCode.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository repository)
        {
            _userRepository = repository;
        }

        public CreateUserResponse Create(CreateUserRequest createRequest)
        {
            var response = new CreateUserResponse();

            if (ValidateCreateRequest(createRequest))
            {
                var user = _userRepository.GetUserByEmail(createRequest.Email);

                if (user == null)
                {
                    user = new User()
                    {
                        Id = Guid.NewGuid(),
                        Email = createRequest.Email,
                        Name = createRequest.Name,
                    };

                    var hasher = new PasswordHasher();

                    user.Password = hasher.HashPassword(createRequest.Password);

                    _userRepository.AddNew(user);

                    _userRepository.SaveChanges();

                    response.Success = true;
                    response.User = user;
                }
                else
                {
                    response.Error = "A user with that E-mail already exists!";
                }
            }
            else
            {
                response.Error = "Create user request was not valid!";
            }

            return response;
        }

        private bool ValidateCreateRequest(CreateUserRequest request)
        {
            return !string.IsNullOrWhiteSpace(request.Email) && !string.IsNullOrWhiteSpace(request.Name) && !string.IsNullOrWhiteSpace(request.Password);
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest authRequest)
        {
            var response = new AuthenticateResponse();

            if (ValidateAuthenticateRequest(authRequest))
            {
                var user = _userRepository.GetUserByEmail(authRequest.Email);

                if (user != null)
                {
                    var hasher = new PasswordHasher();

                    if (hasher.VerifyHashedPassword(user.Password, authRequest.Password) == PasswordVerificationResult.Success)
                    {
                        response.Success = true;
                        response.UserName = user.Name;
                        response.Token = GenerateToken(user);
                    }
                    else
                    {
                        response.Error = "E-mail or Password were not correct!";
                    }
                }
                else
                {
                    response.Error = "E-mail or Password were not correct!";
                }
            }
            else
            {
                response.Error = "Authenticate user request was not valid!";
            }

            return response;
        }

        private bool ValidateAuthenticateRequest(AuthenticateRequest request)
        {
            return !string.IsNullOrWhiteSpace(request.Email) && !string.IsNullOrWhiteSpace(request.Password);
        }

        public User GetById(Guid id)
        {
            return _userRepository.GetUserById(id);
        }

        private string GenerateToken(User user)
        {
            // A simple token, should use something like a JWT token
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(user.Id.ToString()));
        }
    }
}
