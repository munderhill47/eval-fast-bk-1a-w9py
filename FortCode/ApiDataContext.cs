﻿using Fort.Api.Models;
using FortCode.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FortCode
{
    public class ApiDataContext : DbContext
    {
        private IConfiguration _config;

        public ApiDataContext(IConfiguration config)
        {
            _config = config;
        }

        public ApiDataContext(DbContextOptions<ApiDataContext> options, IConfiguration config) : base(options)
        {
            _config = config;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Bar> Bars { get; set; }
        public DbSet<UserCity> UserCities { get; set; }
        public DbSet<UserBar> UserBars { get; set; }
        public DbSet<SharedBarDto> SharedBars { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(@"Server = localhost\SQLEXPRESS; Database = FortCode; Trusted_Connection = True;");
                optionsBuilder.UseSqlServer(_config.GetConnectionString("DbContext"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserCity>().HasKey(uc => new { uc.UserId, uc.CityId });
            modelBuilder.Entity<UserBar>().HasKey(ub => new { ub.UserId, ub.BarId });

            modelBuilder.Entity<UserCity>()
                .HasOne(uc => uc.User)
                .WithMany(u => u.UserCities)
                .HasForeignKey(uc => uc.UserId);

            modelBuilder.Entity<UserCity>()
                .HasOne(uc => uc.City)
                .WithMany(c => c.UserCities)
                .HasForeignKey(uc => uc.CityId);

            modelBuilder.Entity<UserBar>()
                .HasOne(ub => ub.User)
                .WithMany(u => u.UserBars)
                .HasForeignKey(ub => ub.UserId);

            modelBuilder.Entity<UserBar>()
                .HasOne(ub => ub.Bar)
                .WithMany(u => u.UserBars)
                .HasForeignKey(ub => ub.BarId);

            modelBuilder
                .Entity<SharedBarDto>()
                .ToSqlQuery("SELECT t2.UserId, t1.BarId, t3.Name, ShareCount FROM " +
                "(SELECT BarId, COUNT(*) - 1 as ShareCount FROM UserBars Group by BarId) as t1 " +
                "JOIN UserBars t2 on t2.BarId = t1.BarId " +
                "JOIN Bars t3 on t3.Id = t2.BarId");
        }
    }

    public class PrimitiveDto<T>
    {
        public T Value { get; set; }
    }
}
