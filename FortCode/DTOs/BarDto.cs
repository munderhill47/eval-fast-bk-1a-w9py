﻿using System;

namespace FortCode.DTOs
{
    public class BarDto
    {
        public Guid BarId { get; set; }
        public string Name { get; set; }
        public string FavoriteDrink { get; set; }
    }
}
