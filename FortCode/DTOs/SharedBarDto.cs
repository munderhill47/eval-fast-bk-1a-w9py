﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.DTOs
{
    public class SharedBarDto
    {
        [Key]
        public Guid UserId { get; set; }
        public Guid BarId { get; set; }
        public string Name { get; set; }
        public int ShareCount { get; set; }
    }
}
