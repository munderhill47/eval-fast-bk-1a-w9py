using FortCode.Interfaces;
using FortCode.Repositories;
using FortCode.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FortCode
{
    public class Startup
    {
        private IConfiguration _config;

        public Startup(IConfiguration config)
        {
            _config = config;
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddControllers();

            services.AddDbContext<ApiDataContext>();

            // Configure DI for application services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IBarService, BarService>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IBarRepository, BarRepository>();
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApiDataContext dbContext, ILoggerFactory loggerFactory)
        {
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            app.UseMiddleware<AuthenticationMiddleware>();

            app.UseFileServer()
               .UseRouting()
               .UseEndpoints(endPoints => { endPoints.MapControllers(); });

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}
