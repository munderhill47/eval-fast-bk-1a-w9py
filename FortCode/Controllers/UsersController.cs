﻿using FortCode.Interfaces;
using FortCode.Models.Requests;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controllers
{
    [Route("/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] CreateUserRequest request)
        {
            var response = _userService.Create(request);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error, Request = request });
            }

            return Created("", response);
        }

        [HttpPost]
        [Route("authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequest request)
        {
            var response = _userService.Authenticate(request);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error, Request = request });
            }

            return Ok(response);
        }
    }
}
