﻿using FortCode.Interfaces;
using FortCode.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using System;

namespace FortCode.Controllers
{
    [Route("/cities/{cityId:guid}/[controller]")]
    public class BarsController : ControllerBase
    {
        private IBarService _barService;

        public BarsController(IBarService barService)
        {
            _barService = barService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult ListBars(Guid cityId)
        {
            var userId = HttpContext.GetItem<Guid>("userId");

            var response = _barService.GetListForCity(userId, cityId);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error });
            }

            return Ok(response.Entities);
        }

        [HttpGet]
        [Authorize]
        [Route("/sharedbars")]
        public IActionResult ListSharedBars()
        {
            var userId = HttpContext.GetItem<Guid>("userId");

            var response = _barService.GetSharedListForUser(userId);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error });
            }

            return Ok(response.Entities);
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateBar(Guid cityId, [FromBody] CreateBarRequest request)
        {
            var userId = HttpContext.GetItem<Guid>("userId");

            var response = _barService.Create(userId, cityId, request);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error, Request = request });
            }

            return Created("", response);
        }

        [HttpDelete]
        [Authorize]
        [Route("{id}")]
        public IActionResult DeleteBar(Guid cityId, Guid barId)
        {
            var userId = HttpContext.GetItem<Guid>("userId");

            var response = _barService.Delete(userId, barId);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error });
            }

            return NoContent();
        }
    }
}
