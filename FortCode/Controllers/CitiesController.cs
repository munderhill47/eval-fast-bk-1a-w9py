﻿using FortCode.Interfaces;
using FortCode.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using System;

namespace FortCode.Controllers
{
    [Route("/[controller]")]
    public class CitiesController : ControllerBase
    {
        private ICityService _cityService;

        public CitiesController(ICityService cityService)
        {
            _cityService = cityService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult ListCities()
        {
            var userId = HttpContext.GetItem<Guid>("userId");

            var response = _cityService.GetList(userId);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error });
            }

            return Ok(response.Entities);
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateCity([FromBody] CreateCityRequest request)
        {
            var userId = HttpContext.GetItem<Guid>("userId");

            var response = _cityService.Create(userId, request);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error, Request = request });
            }

            return Created("", response);
        }

        [HttpDelete]
        [Authorize]
        [Route("{cityId}")]
        public IActionResult DeleteCity(Guid cityId)
        {
            var userId = HttpContext.GetItem<Guid>("userId");

            var response = _cityService.Delete(userId, cityId);

            if (!response.Success)
            {
                return BadRequest(new { Message = response.Error });
            }

            return NoContent();
        }
    }
}
