﻿using Fort.Api.Models;
using FortCode.Interfaces;
using FortCode.Respositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace FortCode.Repositories
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(ApiDataContext dbContext) : base(dbContext)
        {
        }

        public User GetUserByEmail(string email)
        {
            return _dbContext.Users.SingleOrDefault(u => EF.Functions.Like(u.Email, email));
        }

        public User GetUserById(Guid userId)
        {
            return _dbContext.Users.SingleOrDefault(u => u.Id == userId);
        }

        public void AddNew(User user)
        {
            _dbContext.Users.Add(user);
        }
    }
}
