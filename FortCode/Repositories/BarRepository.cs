﻿using Fort.Api.Models;
using FortCode.DTOs;
using FortCode.Interfaces;
using FortCode.Respositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FortCode.Repositories
{
    public class BarRepository : RepositoryBase, IBarRepository
    {
        public BarRepository(ApiDataContext dbContext) : base(dbContext)
        {
        }

        public List<BarDto> GetFavoriteBarsInCity(Guid userId, Guid cityId)
        {
            var user = _dbContext.Users.Where(u => u.Id == userId).Include(x => x.UserBars).ThenInclude(uc => uc.Bar).SingleOrDefault();

            return user.UserBars.Select(ub => new BarDto() { BarId = ub.Bar.Id, Name = ub.Bar.Name, FavoriteDrink = ub.FavoriteDrink }).ToList();
        }
        public List<SharedBarDto> GetSharedBarsForUser(Guid userId)
        {
            return _dbContext.SharedBars.Where(sb => sb.UserId == userId && sb.ShareCount > 0).ToList();
        }

        public Bar GetBarById(Guid barId)
        {
            return _dbContext.Bars.SingleOrDefault(b => b.Id == barId);
        }

        public Bar GetBarByNameInCity(Guid cityId, string name)
        {
            return _dbContext.Bars.SingleOrDefault(b => EF.Functions.Like(b.Name, name) && b.CityId == cityId);
        }

        public void AddNew(Guid userId, Bar bar, string favoriteDrink)
        {
            _dbContext.Bars.Add(bar);

            _dbContext.UserBars.Add(new UserBar { UserId = userId, BarId = bar.Id, FavoriteDrink = favoriteDrink });
        }

        public void AddUserAssociationToBar(Guid userId, Guid barId, string favoriteDrink)
        {
            if (_dbContext.UserBars.FirstOrDefault(ub => ub.BarId == barId && ub.UserId == userId) == null)
            {
                _dbContext.UserBars.Add(new UserBar { UserId = userId, BarId = barId, FavoriteDrink = favoriteDrink });
            }
        }
        public void Delete(Guid userId, Bar bar)
        {
            var userBar = _dbContext.UserBars.SingleOrDefault(ub=> ub.UserId == userId && ub.BarId == bar.Id);

            if (userBar != null)
            {
                _dbContext.UserBars.Remove(userBar);

                SaveChanges();
            }

            if (!_dbContext.UserBars.Any(ub => ub.BarId == bar.Id)) // Only delete the Bar when no user is associated with it!
            {
                _dbContext.Bars.Remove(bar);
            }
        }
    }
}
