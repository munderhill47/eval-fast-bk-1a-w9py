﻿using Fort.Api.Models;
using FortCode.Interfaces;
using FortCode.Respositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FortCode.Repositories
{
    public class CityRepository : RepositoryBase, ICityRepository
    {
        public CityRepository(ApiDataContext dbContext) : base(dbContext)
        {
        }

        public List<City> GetFavoriteCities(Guid userId)
        {
            var user = _dbContext.Users.Where(u => u.Id == userId).Include(x => x.UserCities).ThenInclude(uc => uc.City).SingleOrDefault();

            return user.UserCities.Select(uc => uc.City).ToList();
        }

        public City GetCityByNameAndCountry(string name, string country)
        {
            return _dbContext.Cities.SingleOrDefault(c =>  EF.Functions.Like(c.Name, name) && EF.Functions.Like(c.Country, country));
        }

        public City GetCityById(Guid cityId)
        {
            return _dbContext.Cities.SingleOrDefault(c => c.Id == cityId);
        }

        public void AddNew(Guid userId, City city)
        {
            _dbContext.Cities.Add(city);

            _dbContext.UserCities.Add(new UserCity { UserId = userId, CityId = city.Id });
        }

        public void AddUserAssociationToCity(Guid userId, Guid cityId)
        {
            if (_dbContext.UserCities.FirstOrDefault(uc => uc.CityId == cityId && uc.UserId == userId) == null)
            {
                _dbContext.UserCities.Add(new UserCity { UserId = userId, CityId = cityId });
            }
        }
        public void Delete(Guid userId, City city)
        {
            var userCity = _dbContext.UserCities.SingleOrDefault(uc => uc.UserId == userId && uc.CityId == city.Id);

            if (userCity != null)
            {
                _dbContext.UserCities.Remove(userCity);

                SaveChanges();
            }

            if (!_dbContext.UserCities.Any(uc => uc.CityId == city.Id)) // Only delete the City when no user is associated with it!
            {
                _dbContext.Cities.Remove(city);
            }
        }
    }
}
