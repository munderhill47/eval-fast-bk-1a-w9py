﻿using FortCode.Interfaces;

namespace FortCode.Respositories
{
    public class RepositoryBase : IRepository
    { 
        protected ApiDataContext _dbContext { get; set; }

        public RepositoryBase(ApiDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }
    }
}
