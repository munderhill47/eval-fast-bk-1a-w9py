﻿using Fort.Api.Models;
using System;

namespace FortCode.Interfaces
{
    public interface IUserRepository : IRepository
    {
        User GetUserById(Guid userId);
        User GetUserByEmail(string email);

        void AddNew(User user);
    }
}
