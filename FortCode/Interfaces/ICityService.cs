﻿using Fort.Api.Models;
using FortCode.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using System;

namespace FortCode.Interfaces
{
    public interface ICityService
    {
        ListResponse<City> GetList(Guid userId);
        CreateCityResponse Create(Guid userId, CreateCityRequest request);
        DeleteCityResponse Delete(Guid userId, Guid cityId);
    }
}
