﻿using Fort.Api.Models;
using System.Collections.Generic;
using System;
using FortCode.DTOs;

namespace FortCode.Interfaces
{
    public interface IBarRepository : IRepository
    {
        List<BarDto> GetFavoriteBarsInCity(Guid userId, Guid cityId);
        Bar GetBarById(Guid barId);
        Bar GetBarByNameInCity(Guid cityId, string name);
        List<SharedBarDto> GetSharedBarsForUser(Guid userId);

        void AddNew(Guid userId, Bar bar, string favoriteDrink);
        void AddUserAssociationToBar(Guid userId, Guid barId, string favoriteDrink);

        void Delete(Guid userId, Bar bar);
    }
}
