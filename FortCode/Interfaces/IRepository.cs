﻿namespace FortCode.Interfaces
{
    public interface IRepository
    {
        int SaveChanges();
    }
}
