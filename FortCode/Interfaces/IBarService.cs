﻿using Fort.Api.Models;
using FortCode.DTOs;
using FortCode.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using System;

namespace FortCode.Interfaces
{
    public interface IBarService
    {
        ListResponse<BarDto> GetListForCity(Guid userId, Guid cityId);
        ListResponse<SharedBarDto> GetSharedListForUser(Guid userId);
        CreateBarResponse Create(Guid userId, Guid cityId, CreateBarRequest request);
        DeleteBarResponse Delete(Guid userId, Guid barId);
    }
}
