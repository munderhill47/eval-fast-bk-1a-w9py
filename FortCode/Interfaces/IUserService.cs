﻿using FortCode.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;

namespace FortCode.Interfaces
{
    public interface IUserService
    {
        CreateUserResponse Create(CreateUserRequest request);

        AuthenticateResponse Authenticate(AuthenticateRequest request);
    }
}
