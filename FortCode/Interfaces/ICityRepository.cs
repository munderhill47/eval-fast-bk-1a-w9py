﻿using Fort.Api.Models;
using System.Collections.Generic;
using System;

namespace FortCode.Interfaces
{
    public interface ICityRepository : IRepository
    {
        List<City> GetFavoriteCities(Guid userId);
        City GetCityByNameAndCountry(string name, string country);
        City GetCityById(Guid cityId);

        void AddNew(Guid userId, City city);
        void AddUserAssociationToCity(Guid userId, Guid cityId);

        void Delete(Guid userId, City city);
    }
}